//listen gor form submit
document.getElementById('myForm').addEventListener('submit',saveBookmark);

function saveBookmark(e){

 var siteName = document.getElementById("siteName").value;
 var siteUrl  = document.getElementById("siteUrl").value;

 var bookmark = {
  name: siteName,
  url: siteUrl
 };

 console.log(bookmark);

 var bookmarks = localStorage.getItem("bookmarks")
 if(bookmarks === null){
  bookmarks = "[]";
 }
 bookmarks = JSON.parse(bookmarks)
 bookmarks.push(bookmark);
 localStorage.setItem("bookmarks",JSON.stringify(bookmarks));

fetchBookmarks();
//prevent from submitting/reloading
 e.preventDefault();
}

function deleteBookmark(url){
 console.log(url);
 var bookmarks = JSON.parse(localStorage.getItem("bookmarks"))
 for(var i=0;i<bookmarks.length;i++){
  if(bookmarks[i].url == url){
   bookmarks.splice(i,1);
   break;
  }
 }
 localStorage.setItem("bookmarks",JSON.stringify(bookmarks));
 fetchBookmarks();
}

function fetchBookmarks(){
  var bookmarks = JSON.parse(localStorage.getItem("bookmarks"))

  var results = document.getElementById("bookmarksResults")
  results.innerHTML = "";
  for(var i=0;i<bookmarks.length;i++){
   var name = bookmarks[i].name
   var url = bookmarks[i].url
   results.innerHTML += '<div class="list-item"> <h3> '
                       +' <a target="_blank" href="'+url+'"> '+name+' ('+url+') </a> '
                       +' <a onclick="deleteBookmark(\''+url+'\')" class="align-right" href="#"> <i class="fa fa-trash"></i></a> '
                       +'</h3> </div>';
  }
}
